//
//  ViewController.swift
//  NeedSomeWeather
//
//  Created by Sasha Evtushenko on 7/8/19.
//  Copyright © 2019 Sasha Evtushenko. All rights reserved.
//

import UIKit
import Foundation


struct WeatherObject {
    let date: String
    let temperature: Double
}

class WeatherViewController: UITableViewController {
    
    let APP_ID = "&appid=bf82ecb3efa2f5511ade034df37f2ce3"
    let urlString = "http://api.openweathermap.org/data/2.5/forecast?q=Baranavichy"
    var weatherObjects = [WeatherObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.reloadData()
        
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Here's weather for you place. Check it out"
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return weatherObjects.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WeatherCell", for: indexPath) as! WeatherCell
        
        cell.dateLabel.text = weatherObjects[indexPath.row].date
        cell.weatherLabel.text = String(weatherObjects[indexPath.row].temperature)
        
        return cell
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getWeather()
    }
    
    func getWeather() {
        if let url = URL(string: urlString + APP_ID) {
            let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
                if error != nil {
                    print(error!)
                } else {
                    if let content = data {
                        do {
                            let jsonResult = try JSONSerialization.jsonObject(with: content, options: JSONSerialization.ReadingOptions.mutableContainers) as AnyObject
                            
                            if let allObjects = jsonResult["list"] as? NSArray {
                                for index in 1...allObjects.count {
                                    if let date = ((jsonResult["list"] as? NSArray)?[index - 1] as? NSDictionary)?["dt_txt"] as? String {
                                        
                                        if let temperature = (((jsonResult["list"] as? NSArray)?[index - 1] as? NSDictionary)?["main"] as? NSDictionary)?["temp"] as? Double {
                                                self.weatherObjects.append(WeatherObject(date: date, temperature: temperature - 273.15))
                                        }
                                    }
                                }
                                DispatchQueue.main.sync {
                                    self.tableView.reloadData()
                                }
                                print(self.weatherObjects)
                            }
                        } catch {
                            print("Error JSON Serialization...")
                        }
                    }
                }
            }
            task.resume()
        }
    }
    
    
    
}

